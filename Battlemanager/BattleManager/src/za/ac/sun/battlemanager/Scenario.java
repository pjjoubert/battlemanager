package za.ac.sun.battlemanager;

import java.util.Calendar;
import java.util.Date;

import android.provider.MediaStore.Images;

public class Scenario {
	
	private Library library;
	private Map map;
	private TurnTracker turnTracker;
	
	public String adventureName;
	public String campaignName;
	public Date dateCreated;
	public Date dateLastPlayed;
	public Images thumbnail;
	
	public Scenario(){
		//This constructor is used when a scenario is created for the first time
		library = new Library();
		//map = new Map();
		turnTracker = new TurnTracker();
		
		Calendar calendar = Calendar.getInstance();
		//Just using hard-coded defaults for the moment. These values have to be captured 
		adventureName = "The Grand Adventure Of " + calendar.getTime();
		campaignName = "The glorious campaign of " + calendar.getTime();
		dateCreated = calendar.getTime();
		dateLastPlayed = dateCreated;
	}
	
	public Scenario(String fileName){
		//This constructor is used when loading a scenario from file.
		
	}
	
	

}
