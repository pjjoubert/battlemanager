package za.ac.sun.battlemanager;

import java.util.List;

import android.graphics.Point;

public class Character extends ScenarioLibraryElement{
	
	public String type;
	public String name;
	public Point worldCoordinates;
		
	public List<Effect> activeEffectsList;
	public List<Effect> sustainedEffectsList;
	public List<Effect> favouritesList;
	public String status; //playing, waiting, delaying -perhaps more?
	
	public Character(){
		
	}
	
	public void updatePosition(int newX, int newY){
		//sets the new position of the character;
		//important note - DO NOT SAVE THE CHARACTER POSITION IN THE CHARACTER CLASS!
		//The position of the character must be an element of a saved scenario
	}
	
	public void updateState(String status){
		this.status = status;
	}
	
	public void updateFavouritesList(){
		//check counts of spells/effects used and bubble to most used ones to the top
	}
	
	public void updateEffectsList(){
		
	}

}
