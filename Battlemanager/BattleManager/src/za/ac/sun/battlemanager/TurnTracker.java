package za.ac.sun.battlemanager;

import java.util.List;
import java.util.Stack;

public class TurnTracker {
	
	private int turnCounter; //tracks who should play next;
	
	private Stack<Character> immediateStack; //stacks players who plays immediate actions
	
	private List<Character> charactersOnMap; //Will I need this if I have the next?
	public List<Character> charInitiativeOrderList;
	public List<Character> currentTurnList;
	
	public Character getNext(){		
		return charInitiativeOrderList.get(0);
	}
	
	public void updatecharInitiativeOrder(){
		charInitiativeOrderList = null;
		//update it ...somehow. Made more sense when designing
		//and there, kids, we have the greatest argument against designing late at night while high on caffeine
	}
	
	public void updatecurrentTurnList(){
		
	}
	
	public void immediateAction(Character character){
		immediateStack.push(character);
	}
	
	

}
