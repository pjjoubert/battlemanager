package za.ac.sun.battlemanager;

public class Position {
	public int x;
	public int y;
	
	public Position(){
		x =0;
		y= 0;
	}
	
	public Position(int x, int y){
		this.x = x;
		this.y = y;
	}
	
	public Position getPostion(){
		return this;
	}
	
	public int getX(){
		return x;		
	}
	
	public int getY(){
		return y;
	}
	
	public void setX(int x){
		this.x = x;
	}
	
	public void setY(int y){
		this.y = y;
	}
	
	public void setPostion(int x, int y){
		this.x = x;
		this.y = y;
	}

}
