package za.ac.sun.battlemanager;


import android.graphics.Point;
import android.graphics.PointF;

public class Item extends ScenarioLibraryElement{
	//baseclass for all map and library elements

	public int image;
	public String description;
	public String name;
	public PointF coordinates;

	
	public Item(){
		this.name = "An Object";
		this.description = "This is a very plain object. Ordinary in every way";
		this.image = R.drawable.ic_launcher;//R.drawable.ic_launcher;
		this.coordinates = new PointF();
		this.coordinates.x = 10;
		this.coordinates.y = 10;
	}
	
	public Item(PointF coords){
		this.name = "An Object";
		this.description = "This is a very plain object. Ordinary in every way";
		this.image = R.drawable.ic_launcher;//R.drawable.ic_launcher;
		this.coordinates = new PointF();
		this.coordinates = coords;
	}
}
