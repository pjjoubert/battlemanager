package za.ac.sun.battlemanager;

import android.provider.MediaStore.Images;

public class MapElement {
	public String name;
	public String description;
	public Images image;
	public Position position; //this needs to be an x-y coordinarte
	
	public MapElement(){
		name = "Common Object";
		description = "A Common object.";
		position = new Position(0,0);
	}
	
	public MapElement(String name, String description, Images image, Position position){
		this.name = name;
		this.description = description;
		this.image = image;
		this.position = position;		
	}
	
	public MapElement(String name, String desctiption){
		this.name = name; 
		this.description = description;
	}

	public void updateImage(String newFileName){
		
	}
	
	

}
