package za.ac.sun.battlemanager;


import android.app.Activity;
import android.graphics.PointF;
import android.os.Bundle;
import android.support.v4.view.GestureDetectorCompat;
import android.util.Log;
import android.view.DragEvent;
import android.view.GestureDetector.OnDoubleTapListener;
import android.view.GestureDetector.OnGestureListener;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.View.OnDragListener;
import android.widget.Toast;

public class ScenarioActivity extends Activity implements OnDoubleTapListener,
		OnGestureListener, OnDragListener {

	private GestureDetectorCompat mDetector;
	private ScaleGestureDetector scaleGestureDetector;

	protected static final String DEBUG_TAG = "DEBUG";
	Map scenarioMap;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// setContentView(R.layout.activity_scenario);
		
		scenarioMap = new Map(this);
		setContentView(scenarioMap);
		mDetector = new GestureDetectorCompat(this, this);
		scaleGestureDetector = new ScaleGestureDetector(this, new ScaleListener());
		// Set the gesture detector as the double tap
		// listener.
		mDetector.setOnDoubleTapListener(this);

	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		super.onTouchEvent(event);
		this.mDetector.onTouchEvent(event);
		// Be sure to call the superclass implementation		
		scaleGestureDetector.onTouchEvent(event);
		return true;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_scenario, menu);
		return true;
	}

	@Override
	public boolean onDown(MotionEvent e) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
			float velocityY) {
		// TODO Auto-generated method stub
		return false;
	}
	

	@Override
	public void onLongPress(MotionEvent e) {
		// TODO Auto-generated method stub
		PointF longPressCoords = new PointF( e.getRawX(), e.getRawY() );
		//scenarioMap.showSelectedItemMenu(longPressCoords);
	}

	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
			float distanceY) {
		// TODO Auto-generated method stub
		Log.d("GESTURE TAG", "THEY KEEP SCROLING");
		
		scenarioMap.moveOffset(new PointF(distanceX, distanceY));
		//scenarioMap.forceDraw();
		scenarioMap.postInvalidate();
		return true;
	}

	@Override
	public void onShowPress(MotionEvent e) {
		//usually used for actions like highlighting an items

	}

	@Override
	public boolean onSingleTapUp(MotionEvent e) {
		//select map item
		//toast("HIT!!");
		PointF tapCoords = new PointF( e.getRawX(), e.getRawY() );
		scenarioMap.getWorldCoord(tapCoords);
		if (scenarioMap.itemAtCoords(tapCoords)){
			toast("There is an item at the tapped coordinates");
		}
		else {
			toast("Nothing here at " + scenarioMap.getWorldCoord(tapCoords).x + ", " + scenarioMap.getWorldCoord(tapCoords).y + " except rocks and mice");
		}
		
		return true;
	}

	@Override
	public boolean onSingleTapConfirmed(MotionEvent e) {
		// select map item
		PointF longPressCoords = new PointF( e.getRawX(), e.getRawY() );
		//scenarioMap.selectMapElement(longPressCoords);
		return true;
	}
	
	@Override
	public boolean onDoubleTap(MotionEvent e) {
		// TODO Auto-generated method stub
		scenarioMap.center();
		return true;
	}

	@Override
	public boolean onDoubleTapEvent(MotionEvent e) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean onDrag(View arg0, DragEvent arg1) {
		// get the element
		//get distance.
		//change element map offset with distance
		return false;
	}

	private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {
		@Override
	    public boolean onScale(ScaleGestureDetector detector) {
			float zoom = (float)scenarioMap.getZoomFactor();
			zoom*= detector.getScaleFactor();

	      // Don't let the object get too small or too large.
			zoom = Math.max(0.1f, Math.min(zoom, 5.0f));
			scenarioMap.setZoomFactor(zoom);
	      return true;
	    }
	}
	
	private void toast(String text){
		Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
	}

}
