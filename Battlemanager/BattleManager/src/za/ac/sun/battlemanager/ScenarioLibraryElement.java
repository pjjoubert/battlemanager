package za.ac.sun.battlemanager;

import java.util.List;

import android.provider.MediaStore.Images;

public class ScenarioLibraryElement {
	public List<String> namesList; //List of alternative name -  used to uniquely Identify each element on the map
	public List<Images> imageList; //list of alternative images - variety for the map
	public String description; //What it says on the can, Short description of the element in question
	
	public ScenarioLibraryElement(){
		
	}
	
	public List<String> getNamesList(){
		return namesList;
	}
	
	public List<Images> getImageList(){
		return imageList;
	}
	
	public String getDescription(){
		return description;
	}
}
