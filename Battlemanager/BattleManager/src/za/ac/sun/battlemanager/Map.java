package za.ac.sun.battlemanager;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PointF;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.Toast;

public class Map extends SurfaceView implements SurfaceHolder.Callback {
	Context gContext;
	MapThread _thread;
	//manager data structures
	private ArrayList<Zone> activeZonesOnMapList;
	private ArrayList<Effect> activeEffectsOnMapList;
	//private List<Item> itemsOnMapList;
	private ArrayList<Item> itemsOnMapList;
	private ArrayList<Terrain> terrainOnMapList;
	private ArrayList<Character> charactersOnMap;
	
	private PointF offset = new PointF(0.0f, 0.0f); //offset from screen coordinates and actual world coordinates
	
	private Bitmap bmp;
	private int cellHeight;
	private int cellWidth;
	private float zoomFactor = 1;
	//debugging coordinates
	private PointF debugWorldCoordinates1;
	private PointF debugWorldCoordinates2;
	private PointF debugWorldCoordinates3;
	

	Paint paint;
	
	public float getZoomFactor() {
		return zoomFactor;
	}

	public void setZoomFactor(float zoomFactor) {
		this.zoomFactor = zoomFactor;
		invalidate();
	}
	
	public Point getWorldCoord(PointF ScreenCoord ){
		
		PointF returnValue = ScreenCoord;
		returnValue.offset(offset.x, offset.y);
		
		Point returnPoint = new Point((int)Math.floor(returnValue.x/cellWidth), (int)Math.floor(returnValue.y/cellHeight));
		
		return returnPoint;
	}
	
	public PointF getScreenCoord(PointF worldCoord){
		PointF returnValue = new PointF();
		returnValue.offset(-offset.x, -offset.y);
		returnValue.offset(worldCoord.x * cellWidth, worldCoord.y * cellHeight);
		return returnValue;
	}
	
	public void setOffset(PointF delta){		
		offset.x = delta.x;
		offset.y = delta.y;		
	}

	public void moveOffset(PointF delta)
	{
		offset.offset(delta.x, delta.y);		
	}
	
	public void center() {
		// TODO Auto-generated method stub
		//centers the map on the first player in the party
		//offset.x = charactersOnMap.get(1).offset.x;
		//offset.x = charactersOnMap.get(1).offset.y;
	}
	
	public boolean itemAtCoords(PointF tappedScreenCoords) {
		
		Point worldCoords = getWorldCoord(tappedScreenCoords);
		//run through the lists and look for item at with the coordinates suppplied
		boolean thereIsAnItem = false;
		/*for (int itemCount =0; itemCount<itemsOnMapList.length; itemCount++){
			if (thereIsAnItem){ 
				thereIsAnItem=true;
			}
		}
		for (int zoneCount =0; zoneCount<itemsOnMapList.length; zoneCount++){
			if (thereIsAnItem){ 
				thereIsAnItem=true;
			}
		}
		for (int effectCount =0; effectCount<itemsOnMapList.length; effectCount++){
			if (thereIsAnItem){ 
				thereIsAnItem=true;
			}
		}
		for (int characterCount =0; characterCount<itemsOnMapList.length; characterCount++){
			if (thereIsAnItem){ 
				thereIsAnItem=true;
			}
		}*/
		return thereIsAnItem;
	}
	
	private void drawGrid(Canvas canvas, float xOffset, float yOffset){

		//int xBlocks;
		//int yBlocks;
		//SurfaceHolder holder = getHolder();
		//Rect rect = holder.getSurfaceFrame(); //this could be passed as a paramenter and save a method call.

		float screenWidth = canvas.getWidth() / zoomFactor;
		float screenHeight = canvas.getHeight() / zoomFactor;
		
		float blockWidth = cellWidth;
		float blockHeight = cellHeight;
		
		float xcount = (screenWidth/blockWidth)+1; //get the number of blocks on the screen 
		float ycount = (screenHeight/blockHeight)+1;
		
		float gridOriginX= xOffset % blockWidth;
		float gridOriginY= yOffset % blockHeight;
		
		//set a starting point for the the grid depending on the offset value
		if (xOffset > 0){
			gridOriginX = gridOriginX - blockWidth;
		}		
		if (yOffset > 0){
			gridOriginY = gridOriginY - blockHeight;
		}				
		//draw the grid
		for (int x =0; x < xcount; x++){			
			canvas.drawLine(x * blockWidth + gridOriginX, 0, x * blockWidth + gridOriginX, screenWidth, paint);
		}
		for (int y =0; y < ycount; y++){			
			canvas.drawLine(0, y * blockHeight + gridOriginY, screenWidth, y * blockHeight + gridOriginY, paint);			
		}
		//add the mapItems to the map
		//scaleGestureDetector takes care of scaling of images placed on the map - #winning
		for(int itemIndex=0; itemIndex < itemsOnMapList.size(); itemIndex++){
			Bitmap image = BitmapFactory.decodeResource(this.getResources(), itemsOnMapList.get(itemIndex).image );
			//canvas.drawBitmap(image, itemsOnMapList.get(itemIndex).coordinates.x, itemsOnMapList.get(itemIndex).coordinates.y, paint);
			canvas.drawBitmap(image, getScreenCoord(itemsOnMapList.get(itemIndex).coordinates).x, getScreenCoord(itemsOnMapList.get(itemIndex).coordinates).y, paint);
		}
		
/*		Bitmap image = BitmapFactory.decodeResource(this.getResources(),  R.drawable.ic_launcher);
		PointF coords = getScreenCoord(debugWorldCoordinates1);
		canvas.drawBitmap(image, coords.x, coords.y, paint);
		canvas.drawBitmap(image, getScreenCoord(debugWorldCoordinates2).x, getScreenCoord(debugWorldCoordinates2).y, paint);
		canvas.drawBitmap(image, getScreenCoord(debugWorldCoordinates3).x, getScreenCoord(debugWorldCoordinates3).y, paint);*/
	}
	
	public void addItem(Item item, Point point){
		//adds an item to the map at a specified point
		//Item newItem = new Item(item, point);
		//itemsOnMapList.add(newItem);
	}
		
	public Map(Context context) {
		super(context);
		debugWorldCoordinates1 = new PointF(10,10);
		debugWorldCoordinates2 = new PointF(7,12);
		debugWorldCoordinates3 = new PointF(33,10);
		try {

			gContext = context;
			// TODO Auto-generated constructor stub
			getHolder().addCallback(this);		
			paint = new Paint();
			itemsOnMapList = new ArrayList<Item>();
			itemsOnMapList.add(new Item()); //USing a hardcoded item for now, will use the addItem method later
			itemsOnMapList.add( new Item(debugWorldCoordinates1));
			itemsOnMapList.add( new Item(debugWorldCoordinates2));
			itemsOnMapList.add( new Item(debugWorldCoordinates3));
		}
		catch(Exception ex){
			//Log.e("ERROR", ex.getMessage());
			System.out.println("The following error occured " + ex.getMessage());
		}
	}
	
	private void update(Canvas canvas){
		//performs all the background actions like moving timing etc.
		
		drawGrid(canvas, -offset.x, -offset.y);
	}

	@Override
	public void onDraw(Canvas canvas){
		super.onDraw(canvas);
		canvas.save();
		canvas.scale(zoomFactor, zoomFactor);
		canvas.drawColor(Color.WHITE);	

		update(canvas);
		canvas.restore();
	}
		
	//some life-simplyfying tools
	private void toast(String text){
		Toast.makeText(gContext, text, Toast.LENGTH_LONG).show();
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		
		setWillNotDraw(false);
		
		
		this.cellHeight = 50;
		this.cellWidth = 50;
		
		_thread = new MapThread(getHolder(), this);
		_thread.setRunning(true);
		_thread.start();
		
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		// TODO Auto-generated method stub
		try{
			_thread.setRunning(false);
			_thread.join();
		}
		catch(Exception ex){
			Log.d("ERROR_TAG", "Error caught in surfaceDestroyed " + ex.toString());
		}
		
	}
	
	private class MapThread extends Thread{
		private SurfaceHolder _surfaceHolder;
		private Map _map;
		private boolean _run = false;
		
		public MapThread(SurfaceHolder surfaceHolder, Map map){
			_surfaceHolder = surfaceHolder;
			_map = map;
		}
		
		public void setRunning(boolean run){
			_run = run;
		}
		
		@Override
		public void run(){
			Canvas c;
			while(_run){
				c= null;
				try{
					c = _surfaceHolder.lockCanvas(null);
					synchronized(_surfaceHolder){
						//TODO insert methods to update positions of items in onDraw
						postInvalidate();
					}
				}
				finally{
					if (c!=null){
						_surfaceHolder.unlockCanvasAndPost(c);
					}
				}
			}
		}
	}




	
}
