package za.ac.sun.battlemanager;

import java.util.List;

public class Library {
	private List<Character> npcList;
	private List<Zone> zoneList;
	private List<Effect> effectsList;
	private List<Terrain> terrainList;
	private List<Item> itemsList;
	private List<Character> playerList;
	
	public Library(){
		//build the private lists based on the npc, character, effects, terran and items files available
	}
	
	public List<Character> getNPCList(){
		return npcList;
	}
	
	public List<Zone> getZoneList(){
		return zoneList;
	}
	
	public List<Effect> getEffectsList(){
		return effectsList;
	}
	
	public List<Terrain> getTerrainList(){
		return terrainList;
	}
	
	public List<Item> getItemsList(){
		return itemsList;
	}
	
	public List<Character> getPlayerList(){
		return playerList;
	}

}
